package com.nplekhanov.j4cs.demo;

import java.nio.ByteBuffer;

public class DirectSharedBufferTest {
    private static ByteBuffer buffer;

    private static native void exchange(String x);

    public static void init(ByteBuffer buffer) {
        DirectSharedBufferTest.buffer = buffer;
    }

    public static void testExchange() {
        System.out.println("Expected 42: " + buffer.getInt(0));
        buffer.putInt(0, 777);
        exchange("fuuu");
        System.out.println("Expected 256: " + buffer.getInt(0));
    }
}
