package com.nplekhanov.j4cs.demo;

public class CallbackTest {
    private static native void helloClr();
    public static void testCallBack() {
        try {
            helloClr();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
