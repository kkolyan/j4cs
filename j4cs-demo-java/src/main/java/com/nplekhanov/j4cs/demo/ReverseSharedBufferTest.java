package com.nplekhanov.j4cs.demo;

import java.nio.ByteBuffer;

public class ReverseSharedBufferTest {
    private static ByteBuffer buffer;

    private static native void exchange();

    public static void init(ByteBuffer buffer) {
        ReverseSharedBufferTest.buffer = buffer;
    }

    public static void testExchange() {
        System.out.println("Expected 42: " + buffer.getInt());
        buffer.clear();
        buffer.putInt(777);
        buffer.flip();
        exchange();
        System.out.println("Expected 256: " + buffer.getInt());
    }
}
