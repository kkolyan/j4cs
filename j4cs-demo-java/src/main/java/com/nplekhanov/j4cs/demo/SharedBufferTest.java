package com.nplekhanov.j4cs.demo;

import java.nio.ByteBuffer;

public class SharedBufferTest {
    public static void exchange(ByteBuffer buffer) {
        System.out.println("Expected 42: " + buffer.getInt());
        buffer.clear();
        buffer.putInt(777);
        buffer.flip();
    }
}
