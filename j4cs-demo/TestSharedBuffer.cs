using System;
using java.nio;
using net.sf.jni4net.jni;
using Object = java.lang.Object;

namespace j4cs_demo
{
    public class TestSharedBuffer
    {
        public static void testSharedBuffer(JNIEnv env)
        {
            Console.WriteLine("testSharedBuffer");
            var clazz = env.FindClass("com/nplekhanov/j4cs/demo/SharedBufferTest");

            var test1 = clazz.getDeclaredMethod("exchange", new[] {ByteBuffer._class});
            var buffer = ByteBuffer.allocateDirect(1024 * 1024 * 64);
            //TODO need to access direct buffers on C# side using direct memory access to avoid JNI overhead
            buffer.putInt(42);
            buffer.flip();
            test1.invoke(null, new Object[] {buffer});
            Console.WriteLine("Expected 777: " + buffer.getInt());
        }
    }
}