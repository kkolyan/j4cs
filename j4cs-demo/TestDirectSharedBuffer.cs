using System;
using System.Runtime.InteropServices;
using java.lang;
using java.nio;
using net.sf.jni4net.jni;
using net.sf.jni4net.utils;
using Object = java.lang.Object;
using String = java.lang.String;

namespace j4cs_demo
{
    public class TestDirectSharedBuffer
    {
        private static IntPtr bufferAddress;
        private static JNIEnv _env;

        private static void exchangeCs(IntPtr envPtr, IntPtr a, JniLocalHandle b)
        {
            var x = Convertor.FullJ2C<String>(_env, b);
            Console.WriteLine("Expected 777: " + Marshal.ReadInt32(bufferAddress));
            Console.WriteLine("Expected fuuu: " + x);
            Marshal.WriteInt32(bufferAddress, 256);
        }

        public static void testDirectSharedBuffer(JNIEnv env)
        {
            _env = env;
            Console.WriteLine("testDirectSharedBuffer");
            var method = JNINativeMethod.Create(typeof(TestDirectSharedBuffer), "exchange", "exchangeCs", "(Ljava/lang/String;)V");
            var clazz = env.FindClass("com/nplekhanov/j4cs/demo/DirectSharedBufferTest");
            unsafe
            {
                env.RegisterNatives(clazz, &method, 1);
            }

            var buffer = ByteBuffer.allocateDirect(1024 * 1024 * 64);
            buffer.order(ByteOrder.LITTLE_ENDIAN);//TODO: how to choose byte order?

            bufferAddress = env.GetDirectBufferAddress(buffer);

            clazz.getDeclaredMethod("init", new[] {ByteBuffer._class})
                .invoke(null, new Object[] {buffer});

            
            Marshal.WriteInt32(bufferAddress, 42);
            clazz.getDeclaredMethod("testExchange", new Class[0])
                .invoke(null, new Object[0]);
        }
    }
}