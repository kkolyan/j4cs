using System;
using java.lang;
using java.nio;
using net.sf.jni4net.jni;
using Object = java.lang.Object;

namespace j4cs_demo
{
    public class TestReverseSharedBuffer
    {
        private static ByteBuffer testReverseSharedBuffer_buffer;

        private static void testReverseSharedBuffer_exchange()
        {
            Console.WriteLine("Expected 777: " + testReverseSharedBuffer_buffer.getInt());
            testReverseSharedBuffer_buffer.clear();
            testReverseSharedBuffer_buffer.putInt(256);
            testReverseSharedBuffer_buffer.flip();
        }

        public static void testReverseSharedBuffer(JNIEnv env)
        {
            Console.WriteLine("testReverseSharedBuffer");
            var method = JNINativeMethod.Create(typeof(TestReverseSharedBuffer), "exchange", "testReverseSharedBuffer_exchange", "()V");
            var clazz = env.FindClass("com/nplekhanov/j4cs/demo/ReverseSharedBufferTest");
            unsafe
            {
                env.RegisterNatives(clazz, &method, 1);
            }

            testReverseSharedBuffer_buffer = ByteBuffer.allocateDirect(1024 * 1024 * 64);

            clazz.getDeclaredMethod("init", new[] {ByteBuffer._class})
                .invoke(null, new Object[] {testReverseSharedBuffer_buffer});

            testReverseSharedBuffer_buffer.putInt(42);
            testReverseSharedBuffer_buffer.flip();
            clazz.getDeclaredMethod("testExchange", new Class[0])
                .invoke(null, new Object[0]);
        }
    }
}