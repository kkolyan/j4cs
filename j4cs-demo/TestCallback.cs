using System;
using java.lang;
using net.sf.jni4net.jni;
using Object = java.lang.Object;

namespace j4cs_demo
{
    public static class TestCallback
    {
        public static void testCallBack(JNIEnv env)
        {
            Console.WriteLine("testCallBack");
            var method = JNINativeMethod.Create(typeof(TestCallback), "helloClr", "helloClrCs", "()V");
            var clazz = env.FindClass("com/nplekhanov/j4cs/demo/CallbackTest");
            unsafe
            {
                env.RegisterNatives(clazz, &method, 1);
            }

            var test1 = clazz.getDeclaredMethod("testCallBack", new Class[0]);
            test1.invoke(null, new Object[0]);
        }

        private static void helloClrCs()
        {
            Console.WriteLine("helloClrCs");
            Console.WriteLine("Hello CLR back!!!11");
        }
    }
}