﻿using java.lang;
using net.sf.jni4net;
using net.sf.jni4net.jni;

namespace j4cs_demo
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var bridgeSetup = new BridgeSetup()
            {
                Verbose = true,
                // I've troubles to run jni4net with latest JDKs (and with OpenJDK builds too)
                JavaHome = "C:/Program Files/Java/jdk1.8.0_211"
            };
            bridgeSetup.AddClassPath("classes");
            var env = Bridge.CreateJVM(bridgeSetup);
            test1(env);
            TestCallback.testCallBack(env);
            TestSharedBuffer.testSharedBuffer(env);
            TestReverseSharedBuffer.testReverseSharedBuffer(env);
            TestDirectSharedBuffer.testDirectSharedBuffer(env);
        }

        private static void test1(JNIEnv env)
        {
            var clazz = env.FindClass("com/nplekhanov/j4cs/demo/SimpleTest");
            var test1 = clazz.getDeclaredMethod("test1", new Class[0]);
            test1.invoke(null, new Object[0]);
        }
    }
}